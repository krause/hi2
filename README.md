Blogdown template
===

This is a functional rblogdown template to use with gitlab pages @mpib.

Check out `config.toml` and `.gitlab-ci.yaml` to see how it works.
